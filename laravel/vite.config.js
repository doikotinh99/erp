import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import collectModuleAssetsPaths from './vite-module-loader.js';
import legacy from '@vitejs/plugin-legacy';

async function getConfig() {
    const paths = [];
    const allPaths = await collectModuleAssetsPaths(paths, 'Modules');

    return defineConfig({
        assetsInclude: ['**/*.xlsx'],
        plugins: [
            laravel({
                input: allPaths,
                refresh: true
            })
        ]
    });
}

export default getConfig();
